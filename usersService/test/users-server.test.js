const restify = require('restify-clients');
const spawn = require('child_process').spawn;
const config = require('../dist/config/config');

const sp = spawn('bash',[__dirname + '/test-server.sh']);

const connectRest = function(){
    return new Promise((resolve, reject) => {
        try {
            resolve(
                restify.createJsonClient({
                    url: 'http://localhost:3011',
                    version: '*'
                })
            );
        } catch (err) {
            reject(err);
        }
    }).then(client => {
        client.basicAuth('them', 'D4ED43C0-8BD6-4FE2-B358-7C0E230D11EF');
        return client;
    });
}

test(`test to create user`, async function(){
    const client = await connectRest();
    const user = await new Promise((resolve, reject) => {
        client.post('/create-user', {
            firstName: 'Mehmet Egemen',
            familyName: 'Albayrak',
            email: 'mehmetegemenalbayrak@gmail.com',
            birthDate: new Date('04/23/1944'),
            password: '1234' 
        }, (err, req, res, obj) => {
            if (err) return reject(err);
            resolve(obj);
        })
    });

    
    expect(user.firstName).toBe('Mehmet Egemen');
    expect(user.familyName).toBe('Albayrak');
    expect(user.email).toBe('mehmetegemenalbayrak@gmail.com');
    expect(user.birthDate).toEqual(new Date('04/23/1944').toISOString());
    expect(user.password.length).toBeGreaterThan(4);
});

test(`test to destroy user`, async function(){
    const email = 'mehmetegemenalbayrak@gmail.com';
    const client = await connectRest();
    const response = await new Promise((resolve, reject) => {
        client.get('/destroy/' + email, (err, req, res, obj) => {
            if(err) return reject(err);
            resolve(obj);
        });
    });

    expect(response.success).toBe(true);
});