import { Document, Schema, Model, model } from 'mongoose'
import { IUser } from '../interfaces/User'
import bcrypt from 'bcrypt'

export interface IUserModel extends IUser, Document {};

export let UserSchema: Schema = new Schema({
    birthDate: Date,
    firstName: String,
    familyName: String,
    password: String,
    email: String,
    provider: String,
    accessToken: String,
    active: {type: Boolean, default: false}
});

export function passwordHash(password: string): Promise<string> {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, 13, function(err: any, hash: string) {
            if (err) return reject(err);
            resolve(hash);
          });
    });
}

export function compareHashes(data: IComparedHashes) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(data.userPassword, data.recordedPassword, 
            (err, result) => {
                if (err) return reject(err);
                resolve(result);
            });
    });
}

interface IComparedHashes {
    userPassword: string,
    recordedPassword: string
}

export const User: Model<IUserModel> = model<IUserModel>("User", UserSchema);