"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    'USERS_MONGO_URL': process.env.USERS_MONGO_URL,
    'PORT': process.env.PORT || 3004,
    'SECRET': process.env.USERS_SERVICE_SECRET,
    'MAIL_CREDENTIALS': {
        auth: {
            api_key: '41076d24d26c49043919d432c0e87174-b0aac6d0-b93d45d4'
                || process.env.MAIL_API_KEY,
            domain: 'sandboxed97a59eec494f3bb62e1034bc42104f.mailgun.org'
                || process.env.MAIL_DOMAIN
        }
    }
};
