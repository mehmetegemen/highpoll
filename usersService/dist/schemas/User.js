"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const crypto_1 = __importDefault(require("crypto"));
;
exports.UserSchema = new mongoose_1.Schema({
    birthDate: Date,
    firstName: String,
    familyName: String,
    password: String,
    email: String,
    provider: String,
    accessToken: String
});
function passwordHash(password) {
    const hash = crypto_1.default.createHash("sha512");
    hash.update(password);
    return hash.digest('hex');
}
exports.passwordHash = passwordHash;
exports.User = mongoose_1.model("User", exports.UserSchema);
