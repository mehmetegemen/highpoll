"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const User_1 = require("../schemas/User");
const debug_1 = __importDefault(require("debug"));
const config_1 = __importDefault(require("../config/config"));
const error = debug_1.default('users:error');
const log = debug_1.default('users:mongoose');
let db;
function connectDB() {
    return new Promise((resolve, reject) => {
        if (db)
            return resolve(db);
        mongoose_1.default.connect(config_1.default.USERS_MONGO_URL, { useNewUrlParser: true });
        db = mongoose_1.default.connection;
        resolve(mongoose_1.default.connection);
    });
}
exports.connectDB = connectDB;
function createUser(data) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield connectDB();
        }
        catch (err) {
            error(`createUser function throws error, stack trace: ${err.stack}`);
        }
        return new Promise((resolve, reject) => {
            User_1.User.create({
                firstName: data.firstName,
                familyName: data.familyName,
                email: data.email,
                birthDate: data.birthDate,
                password: User_1.passwordHash(data.password),
                provider: data.provider,
                accessToken: data.accessToken
            }, (err, doc) => {
                if (err)
                    return reject(err);
                resolve(doc);
            });
        });
    });
}
exports.createUser = createUser;
;
function updateUser(data) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield connectDB();
        }
        catch (err) {
            error(`updateUser function throws error, stack trace: ${err.stack}`);
        }
        return new Promise((resolve, reject) => {
            // create an object to fill with existing properties of data object
            // this will save us from null fields in database
            let parsedObject = {};
            for (let key of Object.keys(data)) {
                // Prevent email to be updated
                if (key !== 'email' && data[key] !== null) {
                    parsedObject[key] = data[key];
                }
                else if (key === 'password') {
                    parsedObject['password'] = User_1.passwordHash(data.password);
                }
            }
            // Fetch user record by email
            User_1.User.findOneAndUpdate({ email: data.email }, { $set: Object.assign({}, parsedObject) }, { "new": true }, (err, doc) => {
                if (err)
                    return reject(err);
                resolve(doc);
            });
        });
    });
}
exports.updateUser = updateUser;
;
function findUserByEmail(data) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield connectDB();
        }
        catch (err) {
            error(`findUserByEmail function throws error, stack trace: ${err.stack}`);
        }
        return new Promise((resolve, reject) => {
            User_1.User.findOne({ email: data.email }, (err, doc) => {
                if (err)
                    return reject(err);
                resolve(doc);
            });
        });
    });
}
exports.findUserByEmail = findUserByEmail;
;
function findUserById(data) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield connectDB();
        }
        catch (err) {
            error(`findUserById function throws error, stack trace: ${err.stack}`);
        }
        return new Promise((resolve, reject) => {
            User_1.User.findOne({ _id: data._id }, (err, doc) => {
                if (err)
                    return reject(err);
                resolve(doc);
            });
        });
    });
}
exports.findUserById = findUserById;
;
function destroyUser(data) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield connectDB();
        }
        catch (err) {
            error(`destroyUser function throws error, stack trace: ${err.stack}`);
        }
        return new Promise((resolve, reject) => {
            User_1.User.deleteOne({ email: data.email }, (err) => {
                if (err)
                    return reject(err);
                resolve();
            });
        });
    });
}
exports.destroyUser = destroyUser;
;
function sanitize(data) {
    return {
        _id: data._id,
        email: data.email,
        firstName: data.firstName,
        familyName: data.familyName,
        birthDate: data.birthDate,
        provider: data.provider,
        accessToken: data.accessToken
    };
}
exports.sanitize = sanitize;
;
function passwordCheck(data) {
    return __awaiter(this, void 0, void 0, function* () {
        let user;
        try {
            yield connectDB();
            user = yield findUserByEmail(data);
        }
        catch (err) {
            error(`destroyUser function throws error, stack trace: ${err.stack}`);
        }
        if (!user) {
            return { check: false, email: data.email, message: "There is no such a user." };
        }
        else if (user.email === data.email && user.password === User_1.passwordHash(data.password)) {
            return { check: true, email: data.email };
        }
        else {
            return { check: false, email: data.email, message: "Incorrect password." };
        }
    });
}
exports.passwordCheck = passwordCheck;
