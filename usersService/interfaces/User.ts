export interface IUser {
    firstName: string,
    familyName: string,
    birthDate: Date,
    email: string,
    password: string,
    provider?: string,
    accessToken?: string,
    active?: boolean
}

export interface IUserObject {
    firstName?: string,
    familyName?: string,
    birthDate?: Date,
    email?: string,
    password?: string,
    provider?: string,
    accessToken?: string,
    active?: boolean,
    [key: string]: string | undefined | Date | boolean
}