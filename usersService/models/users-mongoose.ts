import mongoose, { Model } from 'mongoose'
import { User, IUserModel, passwordHash, compareHashes } from '../schemas/User'
import debug from 'debug'
import config from '../config/config'
import { IUser, IUserObject } from '../interfaces/User'

const error = debug('users:error');
const log = debug('users:mongoose');

let db: mongoose.Connection;

export function connectDB(): Promise<mongoose.Connection>{
    return new Promise((resolve,reject)=>{
        if (db) return resolve(db);
        mongoose.connect(<string>config.USERS_MONGO_URL,  { useNewUrlParser: true });
        db = mongoose.connection;
        resolve(mongoose.connection);
    });
}

export async function createUser(data: IUserObject): Promise<IUserModel>{
    let hashedPassword = '';
    try {
        await connectDB();
        hashedPassword = await passwordHash(<string>data.password);
    } catch (err) {
        error(`createUser function throws error, stack trace: ${err.stack}`);
    }
    return new Promise<IUserModel>((resolve, reject) => {
        User.create(
            {
                firstName: data.firstName,
                familyName: data.familyName,
                email: data.email,
                birthDate: data.birthDate,
                password: hashedPassword,
                provider: data.provider,
                accessToken: data.accessToken
                // TODO: add "role"
            }, (err: any, doc: IUserModel) => {
                if (err) return reject(err);
                resolve(doc);
            }
        );
    });
};

export async function updateUser(data: IUserObject): Promise<IUserModel>{
    let hashedPassword = '';
    try {
        await connectDB();
        hashedPassword = await passwordHash(<string>data.password)
    } catch (err) {
        error(`updateUser function throws error, stack trace: ${err.stack}`);
    }
    return new Promise<IUserModel>((resolve, reject) => {
        // create an object to fill with existing properties of data object
        // this will save us from null fields in database
        let parsedObject: IUserObject = {};
        for(let key of Object.keys(data)){
            // Prevent email to be updated
            if(key !== 'email' && data[key] !== null){
                parsedObject[key] = data[key];
            } else if (key === 'password') {
                parsedObject['password'] = hashedPassword
            }
        }
        // Fetch user record by email
        User.findOneAndUpdate({email: data.email}, { $set: {...parsedObject} }, 
        {"new": true}, (err: any, doc: IUserModel | null) => {
            if (err) return reject(err);
            resolve(<IUserModel>doc);
        });
    })
};

export async function findUserByEmail(data: IUserObject): Promise<IUserModel>{
    try {
        await connectDB();
    } catch (err) {
        error(`findUserByEmail function throws error, stack trace: ${err.stack}`);
    }
    return new Promise<IUserModel>((resolve, reject) => {
        User.findOne({email: data.email}, (err: any, doc: IUserModel) => {
            if (err) return reject(err);
            resolve(doc);
        });
    });
};

export async function findUserById(data: IUserObject): Promise<IUserModel>{
    try {
        await connectDB();
    } catch (err) {
        error(`findUserById function throws error, stack trace: ${err.stack}`);
    }
    return new Promise<IUserModel>((resolve, reject) => {
        User.findOne({_id: data._id}, (err: any, doc: IUserModel) => {
            if (err) return reject(err);
            resolve(doc);
        });
    });
};

export async function destroyUser(data: IUserObject){
    try {
        await connectDB();
    } catch (err) {
        error(`destroyUser function throws error, stack trace: ${err.stack}`);
    }
    return new Promise<IUserModel>((resolve, reject) => {
        User.deleteOne({email: data.email}, (err: any) => {
            if (err) return reject(err);
            resolve();
        });
    });
};

export function sanitize(data: IUserModel): IUserObject{
    return {
        _id: data._id,
        email: data.email,
        firstName: data.firstName,
        familyName: data.familyName,
        birthDate: data.birthDate,
        provider: data.provider,
        accessToken: data.accessToken,
        active: data.active
    }
};

export async function passwordCheck(data: IUserObject){
    let user, result;
    let hashedPassword = '';
    try {
        await connectDB();
        user = await findUserByEmail(data);
        hashedPassword = await passwordHash(<string>data.password);
        result = await compareHashes(
            {
                userPassword: <string>data.password,
                recordedPassword: <string>user.password 
            }
        )
    } catch (err) {
        error(`destroyUser function throws error, stack trace: ${err.stack}`);
    };
    if(!user){
        return {check: false, email: data.email, message:"There is no such a user."}
    }else if(user.email === data.email && result===true){
        return {check: true, email: data.email}
    }else {
        return {check: false, email: data.email, message: "Incorrect password."}
    }
}

export async function verifyUser(data:IUserObject){
    try {
        await connectDB();
    } catch (err) {
        error(`verifyUser function throws error, stack trace: ${err.stack}`);
    }
    return new Promise<IUserModel>((resolve, reject) => {
        // Fetch user record by email
        User.findOneAndUpdate({email: data.email}, { $set: {active: true} }, 
        {"new": true}, (err: any, doc: IUserModel | null) => {
            if (err) return reject(err);
            resolve(<IUserModel>doc);
        });
    })
}
