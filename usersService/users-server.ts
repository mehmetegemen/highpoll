import util from 'util'
import restify, { Next, RequestHandler } from 'restify'
import * as usersModel from './models/users-mongoose'
import debug from 'debug'
import { IUserObject } from './interfaces/User'
import config from './config/config'

const error = debug('users:error');
const log = debug('users:server');

let server = restify.createServer({
    name: 'Users-Service',
    version: '0.1'
});

server.use(restify.plugins.authorizationParser());
server.use(check);
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser({
    mapParams: true
}));

server.post('/create-user', async function(req: any,res: any, next: any){
    try {
        const doc = await usersModel.createUser(req.body);
        res.send(doc);
        next(false);
    } catch (err) {
        error(`User could not be created: ${err.stack}`);
    }
});

server.post('/update-user', async function(req: any,res: any, next: any){
    try {
        const doc = await usersModel.updateUser(req.body);
        res.send(doc);
        next(false);
    } catch (err) {
        error(`User could not be updated: ${err.stack}`);
    }
});

server.post('/verify', async function(req: any,res: any, next: any){
    try {
        const doc = await usersModel.verifyUser(req.body)
        res.send(doc);
        next(false);
    } catch (err) {
        error(`User could not be verified: ${err.stack}`);
    }
});

server.get('/find-user-by-email/:email', async function(req: any, res: any, next: any){
    try {
        let doc = await usersModel.findUserByEmail({email: req.params.email});
        // Check if document is found
        if (doc === null) {
            // Not found
            // Exit with failure
            next(false);
            return res.send(
                {
                    success: false,
                    message: 'User not found.'
                }
            );
        }
        res.send(usersModel.sanitize(doc));
        next(false);
    } catch (err) {
        error(`User could not be found: ${err.stack}`);
    }
});

server.get('/find-user-by-id/:_id', async function(req: any, res: any, next: any){
    try {
        let doc = await usersModel.findUserById({_id: req.params._id});
        // Check if document is found
        if (doc === null) {
            // Not found
            // Exit with failure
            next(false);
            return res.send(
                {
                    success: false,
                    message: 'User not found.'
                }
            );
        }
        res.send(usersModel.sanitize(doc));
        next(false);
    } catch (err) {
        error(`User could not be found: ${err.stack}`);
    }
});

server.get('/destroy-user/:email', async function(req: any, res: any, next: any){
    try {
        await usersModel.destroyUser(req.params.email);
        res.send(
            {
                success: true,
                message: 'User is destroyed'
            }
        );
        next(false);
    } catch (err) {
        error(`User could not be destroyed: ${err.stack}`);
    }
});

server.post('/password-check', async function(req: any, res: any, next: any){
    try {
        const check = await usersModel.passwordCheck(req.body);
        res.send(check);
        next(false);
    } catch (err) {
        error(`password-check error: ${err.stack}`);
    }
});

server.listen(config.PORT, "0.0.0.0", function () {
    log(server.name + ' listening at ' + server.url);
  })

// Mimic API Key authentication.
var apiKeys = [{
    user: 'them',
    key: '76ba94ee-51f2-4405-a894-50123a83f441'
  }];

function check(req: any, res: any, next: any){
    if (req.authorization) {
        let found = false;
        for (let auth of apiKeys) {
          if (auth.key === req.authorization.basic.password
            && auth.user === req.authorization.basic.username) {
            found = true;
            break;
          }
        }
        if (found) next();
        else {
          res.send(401, new Error("Not authenticated"));
          error('Failed authentication check '
            + util.inspect(req.authorization));
          next(false);
        }
      } else {
        res.send(500, new Error('No Authorization Key'));
        error('NO AUTHORIZATION');
        next(false);
    }
}