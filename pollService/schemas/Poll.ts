import { Document, Schema, Model, model } from 'mongoose'
import { IPoll } from '../interfaces/Poll'

export interface IPollModel extends IPoll, Document {};

const voteSchema: Schema = new Schema({
    label: String,
    count: {type: Number, default: 0}
});

export let PollSchema: Schema = new Schema({
    pollId: String,
    title: Object,
    detail: Object,
    openToVote: Boolean,
    votes: [voteSchema],
    owner: String,
    viewCount: {type: Number, default: 0}
})

export const Poll: Model<IPollModel> = model<IPollModel>("Poll", PollSchema);