import * as pollModel from '../models/poll-mongoose'
import { Request, Response } from 'express';

export default {
    async showPoll (req: Request, res: Response) {
        // find poll by its id
        const poll = await pollModel.findPoll(
            {
                pollId: req.params.pollId
            }
        );

        // Check if poll is null(not found)
        if (poll === null) {
            // poll is not found
            // send error view
            return res.render('error',{message: 'Poll not found'});
        }

        // Poll is found
        // Count total number of votes
        let totalVote = 0;
        for (let vote of poll.votes!) {
            totalVote += vote.count;
        }
        
        // Increment viewCount
        await pollModel.incrementView(poll);

        // Serve it
        res.render('poll', {poll, totalVote}); 
    },
    async vote(req: Request, res: Response) {
        // find poll by its id
        const poll = await pollModel.findPoll(
            {
                pollId: req.params.pollId
            }
        );

        // Check if poll is null(not found)
        if (poll === null) {
            // poll is not found
            // send error view
            return res.render('error',{message: 'Poll not found'});
        }

        // Check if poll is open to vote
        if (poll.openToVote===false) {
            // Poll is not open to vote
            // Exit with failure
            return res.render('error',{message: 'Poll is closed to vote.'});
        }

        await pollModel.vote(poll, req.params.voteIndex);
        res.render('voted');
    }
}