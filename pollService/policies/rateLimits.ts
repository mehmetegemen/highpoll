import expressRateLimit from 'express-rate-limit'

export const showPollLimit = new expressRateLimit({
    // Limit request amount of logins
    windowMs: 1000 * 60, // 1 Minute
    max: 200 // Maximum 200 logins every minute 
});

export const VoteLimit = new expressRateLimit({
    // Limit request amount of logins
    windowMs: 1000 * 60, // 1 Minute
    max: 2 // Maximum 200 logins every minute 
});