import mongoose from 'mongoose'
import { Poll, IPollModel } from '../schemas/Poll'
import debug from 'debug'
import config from '../config/config'
import { IPoll } from '../interfaces/Poll'

const error = debug('poll:error');
const log = debug('poll:mongoose');

let db: mongoose.Connection;

export function connectDB(): Promise<mongoose.Connection>{
    return new Promise((resolve,reject)=>{
        if (db) return resolve(db);
        mongoose.connect(<string>config.POLL_MONGO_URL,  { useNewUrlParser: true });
        db = mongoose.connection;
        resolve(mongoose.connection);
    });
}

export async function vote(poll: IPollModel, voteIndex: number): Promise<IPollModel> {
    try {
        await connectDB();
    } catch (err) {
        error(`Vote error:${err.stack}`)
    }
    let votedObject = Object.assign({}, sanitizePoll(poll.toObject()));
    votedObject.votes![voteIndex].count += 1;
    return new Promise<IPollModel>((resolve, reject) => {
        Poll.findOneAndUpdate({pollId: poll.pollId},{$set: {...votedObject}},
            {"new":true}, (err: any, doc: IPollModel | null) => {
                if (err) return reject(err);
                resolve(<IPollModel>doc);
            });
    });
};

export async function incrementView(poll: IPollModel): Promise<IPollModel> {
    try {
        await connectDB();
    } catch (err) {
        error(`Vote error:${err.stack}`)
    }
    let votedObject = Object.assign({}, sanitizePoll(poll.toObject()));
    votedObject.viewCount! += 1;
    return new Promise<IPollModel>((resolve, reject) => {
        Poll.findOneAndUpdate({pollId: poll.pollId},{$set: {...votedObject}},
            {"new":true}, (err: any, doc: IPollModel | null) => {
                if (err) return reject(err);
                resolve(<IPollModel>doc);
            });
    });
};

export async function findPoll(poll: IPoll): Promise<IPollModel>{
    try {
        await connectDB();
    } catch (err) {
        error(`findPoll error: ${err.stack}`);
    }
    return new Promise<IPollModel>((resolve, reject) => {
        Poll.findOne({pollId: poll.pollId}, 
            (err: any, doc: IPollModel | null) => {
                if (err) return reject(err);
                resolve(<IPollModel>doc);
            });
    })
}

export function sanitizePoll(poll:IPollModel): IPoll {
    return {
        pollId: poll.pollId,
        title: poll.title,
        detail: poll.detail,
        openToVote: poll.openToVote,
        votes: poll.votes,
        owner: poll.owner,
        viewCount: poll.viewCount
    }
}