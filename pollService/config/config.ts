export default {
    'PORT': process.env.POLL_PORT || 3003,
    'SECRET': process.env.POLL_SECRET,
    'POLL_MONGO_URL': process.env.POLL_MONGO_URL
}