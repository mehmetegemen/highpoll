import PollController from './controllers/PollController'
import { Application } from 'express';

import * as rateLimits from './policies/rateLimits'

export default (app: Application) => {
    app.get('/poll/:pollId', rateLimits.showPollLimit, PollController.showPoll);
    app.get('/poll/vote/:pollId/:voteIndex', rateLimits.VoteLimit, PollController.vote);
}