var gulp = require('gulp');

gulp.task('copy_views', function() {
    return gulp.src('./views/*.ejs')
        .pipe(gulp.dest('./dist/views'))
})

gulp.task('copy_css', function() {
    return gulp.src('./public/*.css')
        .pipe(gulp.dest('./dist/public'))
})
 
gulp.task('watch', ['copy_views', 'copy_css'], function() {
    gulp.watch('views/*.ejs', ['copy_views']);
    gulp.watch('public/*.css', ['copy_css']);
});