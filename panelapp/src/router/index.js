import Vue from 'vue'
import Router from 'vue-router'
import store from '../store/index'
import PollManagementPage from '../pages/PollManagementPage.vue'
import AddPoll from '../pages/AddPoll.vue'
import EditPoll from '../pages/EditPoll.vue'
import Login from '../pages/LoginPage.vue'
import IndexPage from '../pages/IndexPage.vue'
import RegisterPage from '../pages/RegisterPage.vue'
import VerifyPage from '../pages/VerifyPage.vue'

Vue.use(Router)


const router = new Router({
    routes: [
      {
        name: 'index',
        path: '/',
        component: IndexPage,
        meta: {
          announcer: 'Home page'
        }
      },
      {
        name: 'poll-management',
        path: '/poll-management',
        component: PollManagementPage,
        meta: {
          announcer: 'Poll management page'
        }
      },
      {
        name: 'add-poll',
        path: '/add-poll',
        component: AddPoll,
        meta: {
          announcer: 'Add Poll page'
        }
      },
      {
        name: 'edit-poll',
        path: '/edit-poll',
        component: EditPoll,
        meta: {
          announcer: 'Edit Poll page'
        }
      },
      {
        name: 'register',
        path: '/register',
        component: RegisterPage,
        meta: {
          announcer: 'Register page'
        }
      },
      {
        name: 'verify',
        path: '/verify',
        component: VerifyPage,
        meta: {
          announcer: 'Verify page'
        }
      },
      {
        name: 'login',
        path: '/login',
        component: Login,
        meta: {
          announcer: 'Login page'
        }
      }
    ]
  })
  
router.beforeEach((to,from,next) => {
  if (store.state.isUserLoggedIn) {
    next()
  } else if(!store.state.isUserLoggedIn && to.name !== 'login' && to.name !== 'register' && to.name!=='verify') {
    next({name: 'login'})
  } else if (store.state.isUserLoggedIn && to.name === 'register') {
    next({name:'poll-management'})
  } else {
    next()
  }
})

export default router