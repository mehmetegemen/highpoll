import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { sync } from 'vuex-router-sync'
import VueAnnouncer from 'vue-announcer';

Vue.use(VueAnnouncer)

Vue.config.productionTip = false
sync(store, router)

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
