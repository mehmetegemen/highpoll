import axios from 'axios'
import store from '../store/index'

export default () => {
    return axios.create({
        baseURL: 'https://www.slickpoll.cc/api/',
        headers: {
          'Content-Type': `application/json`,
          Authorization: `Bearer ${store.state.token}`
        }
    });
}