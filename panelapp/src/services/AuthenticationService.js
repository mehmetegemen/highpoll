import Api from './Api'

export default {
    login (data) {
        return Api().post('/login', data)
    },
    register (data) {
        return Api().put('/register', data)
    },
    verify (token) {
        return Api().get('/verify', {
            params: {
                token
            }
        })
    }
}