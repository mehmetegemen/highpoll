# SlickPoll

![Logo](/Readme/Logo2x.png)

SlickPoll is an Embed.ly compatible poll platform for medium.com. I made it as a side hustle but I don't think I want to maintain it. Project is accessible for screen readers.

It is dockerized with pm2 image, so ready for production. I suggest you to arrange Dockerfiles before deploying. Also you have to customize every `/config/config.ts` file before deploying. You can also infer needed environment variables such as `SECRET` for JWT. Change Axios URL from `/panelapp/src/services/Api.js`.

`/panelapp` contains front end for managing poll records. You can build it with `npm run build` and use it or you can `npm run serve` for development.

Also other services such as mainService and pollService are exposed to outside world contrary to other services which are only accessible internaly. 

## Ports
|Service|Port  |
|--|--|
|mainService|3002|
|pollService|3003|
|usersService|3004|
|panelapp|8080|
|usersDatabase|3006|
|pollsDatabase|3007|

PollsDatabase, usersDatabase and usersService are only accessible internally.

## Tech Stack

#### Front End
- Vue.js
- Vue Router
- Vuex
- Vue announcer (I took accessibility seriously)
- TailwindCSS
- Axios
- Moment.js
- VanillaJS

#### Back End
- Node.js
- Express.js
- Restify
- TypeScript
- MongoDB
- Mongoose
- Jsonwebtoken
- Passport
- Bcrypt
- Joi
- Nodemailer
- Docker
- Docker Compose

## Some Details
- Email verification is done by JWT tokens sent to email.
- Inputs are checked throughly with Joi and some controls in controllers.
- I suggest you to put system behinde nginx, forward ports to domain name.
- MainService needs SECRET env variable to work properly
- I tried to do the front end as accessible as I can on both panel and poll pages.

## ScreenShots

![Add Poll Page](/Readme/AddPollSS1.png)

![Poll management page](/Readme/PollManagementSS1.png)

![Poll example](/Readme/PollSS1.png)

## Licence
MIT