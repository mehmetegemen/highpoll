import mongoose from 'mongoose'
import { generatePollId, IPollModel, Poll } from '../schemas/Poll'
import debug from 'debug'
import { IPoll } from '../interfaces/Poll'
import config from '../config/config'

const error = debug('users:error');
const log = debug('users:mongoose');

let db: mongoose.Connection;

export function connectDB(): Promise<mongoose.Connection>{
    return new Promise((resolve,reject)=>{
        if (db) return resolve(db);
        mongoose.connect(<string>config.MAIN_MONGO_URL,  { useNewUrlParser: true });
        db = mongoose.connection;
        resolve(mongoose.connection);
    });
}

export async function createPoll(data: IPoll): Promise<IPollModel> {
    let generatedPollId: string; // create for later load
    try {
        await connectDB();
        // Buffer to string in hex (its URL safe)
        generatedPollId = (await generatePollId()).toString('hex');
    } catch(err) {
        error(`createPoll error: ${err.stack}`);
    }
    return new Promise<IPollModel>((resolve, reject) => {
        Poll.create(
            {
                pollId: generatedPollId,
                title: {
                    label: data.title!.label,
                    open: data.title!.open
                },
                detail: {
                    text: data.detail!.text,
                    open: data.detail!.open
                },
                openToVote: data.openToVote,
                votes: data.votes,
                viewCount: 0,
                owner: data.owner
            }, (err: any, doc: IPollModel) => {
                if (err) return reject(err);
                resolve(doc);
            }
        )
    })
}

export async function updatePoll(data: IPoll){
    try {
        await connectDB();
    } catch (err) {
        error(`updatePoll function throws error, stack trace: ${err.stack}`);
    }
    return new Promise<IPollModel>((resolve, reject) => {
        // create an object to fill with existing properties of data object
        // this will save us from null fields in database
        let parsedObject: any = {};
        for(let key of Object.keys(data)){
            // Prevent pollId and owner to be updated
            // Omit null fields
            if(key !== 'pollId' && key !== 'owner' && data[key] !== null){
                parsedObject[key] = data[key];
            }
        }
        // Fetch poll by pollId
        Poll.findOneAndUpdate({pollId: data.pollId}, { $set: {...parsedObject} }, 
        {"new": true}, (err: any, doc: IPollModel | null) => {
            if (err) return reject(err);
            resolve(<IPollModel>doc);
        });
    })
}

export async function findPollByPollId(data: IPoll): Promise<IPollModel>{
    try {
        await connectDB();
    } catch (err) {
        error(`findPollByPollId function throws error, stack trace: ${err.stack}`);
    }
    return new Promise<IPollModel>((resolve, reject) => {
        Poll.findOne({pollId: data.pollId}, (err: any, doc: IPollModel) => {
            if (err) return reject(err);
            resolve(doc);
        });
    });
};

export async function destroyPoll(data: IPoll){
    try {
        await connectDB();
    } catch (err) {
        error(`destroyPoll function throws error, stack trace: ${err.stack}`);
    }
    return new Promise<IPollModel>((resolve, reject) => {
        Poll.deleteOne({pollId: data.pollId}, (err: any) => {
            if (err) return reject(err);
            resolve();
        });
    });
};

export async function listPollsByOwner(data: IPoll) {
    // Limit skip and limit properties
    let skip = Number(data.skip); // req.param returns string
    let limit = Number(data.limit);
    if (limit > 20) limit = 20; // Max limit limit
    if (limit < 1) limit = 0; // Lowest limit limit
    if (skip > 2147483645) skip = 2147483645; // Max skip limit by max 32-bit NUMBER
    if (skip < 1) skip = 0; // Lowest skip limit
    try {
        // Connect to database
        await connectDB();
    } catch (err) {
        error(`listPollsByOwner function throws error, stack trace: ${err.stack}`);
    }
    return new Promise<IPollModel[]>((resolve, reject) => {
        Poll.find({owner: data.owner}).skip(skip).limit(limit).exec(function (err, docs) {
            if (err) return reject(err);
            // Resolve documents
            resolve(docs);
        });
    });
}