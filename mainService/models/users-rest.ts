import restify, { IResolvedJsonClient } from 'restify-clients'
import debug from 'debug'
import config from '../config/config'
import { IUser } from '../interfaces/User'
import { IUserModel } from '../schemas/User'
import mailerPlugin from '../plugins/mailer.plugin'
import { IMail } from '../interfaces/Mail' 

const log = debug('main:users-rest');
const error = debug('main:error');

const connectRest = function(){
    return new Promise((resolve, reject) => {
        try {
            resolve(restify.createJsonClient({
                url: <string>config.USERS_SERVICE_URL,
                version: '*'
            }));
        } catch (err) {
            error(`restify client connect error: ${err.stack}`);
        }
    }).then((client: any) => {
        // Server requires API Key
        // Login to server
        client.basicAuth('them', '76ba94ee-51f2-4405-a894-50123a83f441');
        return client;
    })
};

export async function create (data: IUser) {
    try {
        const client = await connectRest();
        return new Promise((resolve, reject) => {
            client.post('/create-user', {
                firstName: data.firstName,
                familyName: data.familyName,
                birthDate: data.birthDate,
                email: data.email,
                password: data.password,
                provider: data.provider,
                accessToken: data.accessToken
            }, (err: any, req: Request, res: Response, obj: IUserModel) => {
                if (err) return reject(err);
                resolve(obj);
            });
        });
    } catch (err) {
        error(`Rest create error: ${err.stack}`);
    }
}

export async function update (data: IUser) {
    try {
        const client = await connectRest();
        return new Promise((resolve, reject) => {
            client.post('/update-user', {
                firstName: data.firstName,
                familyName: data.familyName,
                birthDate: data.birthDate,
                email: data.email,
                password: data.password,
                provider: data.provider,
                accessToken: data.accessToken
            }, (err: any, req: Request, res: Response, obj: IUserModel) => {
                if (err) return reject(err);
                resolve(obj);
            });
        });
    } catch (err) {
        error(`Rest create error: ${err.stack}`);
    }
};

export async function find (data: IUser) {
    try {
        const client = await connectRest();
        return new Promise((resolve, reject) => {
            client.get('/find-user-by-email/' + data.email,
            (err: any, req: Request, res: Response, obj: IUserModel) => {
                if (err) return reject(err);
                resolve(obj);
            });
        });
    } catch (err) {
        error(`Rest create error: ${err.stack}`);
    }
};

export async function findById (data: IUser) {
    try {
        const client = await connectRest();
        return new Promise((resolve, reject) => {
            client.get('/find-user-by-id/' + data.userId,
            (err: any, req: Request, res: Response, obj: IUserModel) => {
                if (err) return reject(err);
                resolve(obj);
            });
        });
    } catch (err) {
        error(`Rest create error: ${err.stack}`);
    }
};

export async function destroy (data: IUser) {
    try {
        const client = await connectRest();
        return new Promise((resolve, reject) => {
            client.get('/destroy-user/' + data.email, 
            (err: any, req: Request, res: Response, obj: IUserModel) => {
                if (err) return reject(err);
                resolve(obj);
            });
        });
    } catch (err) {
        error(`Rest create error: ${err.stack}`);
    }
};

export async function passwordCheck (data: IUser) {
    try {
        const client = await connectRest();
        return new Promise((resolve, reject) => {
            client.post('/password-check', 
            {
                email: data.email,
                password: data.password
            },
            (err: any, req: Request, res: Response, obj: IUserModel) => {
                if (err) return reject(err);
                resolve(obj);
            });
        });
    } catch (err) {
        error(`Rest create error: ${err.stack}`);
    }
};

export async function verify (data: IUser) {
    try {
        const client = await connectRest();
        return new Promise((resolve, reject) => {
            client.post('/verify', {
                email: data.email
            }, (err: any, req: Request, res: Response, obj: IUserModel) => {
                if (err) return reject(err);
                resolve(obj);
            });
        });
    } catch (err) {
        error(`Rest create error: ${err.stack}`);
    }
};

export async function sendVerificationMail(mailData: IMail){
    await mailerPlugin.sendHtml(
        {
          from: 'no-reply@slickpoll.net',
          to: mailData.to,
          subject: 'Account Verification',
          html:`
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="utf-8" />
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <title>Page Title</title>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
            </head>
            <body style="background:#f1f5f8;color:#1d1e22;font-family: 'Open Sans', sans-serif;">
                <div style="width:100%;height:600px;padding-top:40px;">
                    <div style="width:100%;max-width:400px;background:white;margin:80px auto;text-align: center;padding:40px;border-radius: 20px;">
                            <img src="https://i.ibb.co/pv7bCDC/Logo.png" style="width:100%;height:auto;max-width:240px;margin:auto;" alt="Slick Poll logo" />
                            <p>Welcome to SlickPoll, the simplest poll app out there. Now you can benefit of application panel and create polls
                                to inform or get informed by your audience with great polls.
                            </p>
                            <p><b>You have successfuly registered.</b> Now as a last step you need to <b>verify</b> your account by <b>clicking the button below</b>.</p>
                            <a href="${config.HOST_ADDRESS}?#/verify?token=${mailData.token}" style="background:#38C172;padding:4px 40px;font-weight:bold;font-size:1rem;color:white;text-decoration:none;">Verify your account</a>
                        </div>
                </div>
            </body>
            </html>
          `
        }
    )
}