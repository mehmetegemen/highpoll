import { Application } from "express";
import AuthenticationController from './controllers/AuthenticationController'
import UserController from './controllers/UserController'
import PollController from './controllers/PollController'

import isAuthenticated from './policies/isAuthenticated'
import UserControllerPolicy from './policies/UserControllerPolicy'
import AuthenticationControllerPolicy from './policies/AuthenticationControllerPolicy'
import PollControllerPolicy from './policies/PollControllerPolicy'

import * as rateLimits from './policies/rateLimits'

export default (app: Application) => {
    // Authentication Controllers
    app.post('/login', rateLimits.loginLimit, AuthenticationControllerPolicy.login, AuthenticationController.login); // tested
    app.get('/verify', rateLimits.verifyLimit, AuthenticationController.verify);
    
    // User Controllers
    app.put('/register', rateLimits.registerLimit, UserControllerPolicy.register,UserController.register); // tested

    // Poll Controllers
    app.put('/poll/create', isAuthenticated, rateLimits.pollCreateLimit
    , PollControllerPolicy.create, PollController.create) // tested
    app.post('/poll/update', isAuthenticated, rateLimits.pollUpdateLimit
    , PollControllerPolicy.update, PollController.update) // tested
    app.get('/poll/findByPollId/:pollId', isAuthenticated
    , rateLimits.findByPollIdLimit, PollController.findByPollId) // tested
    app.delete('/poll/destroy/:pollId', isAuthenticated, rateLimits.destroyLimit
    , PollController.destroy) // tested
    app.get('/poll/listByOwner/:skip/:limit', isAuthenticated
    , rateLimits.listByOwnerLimit, PollController.listByOwner) // tested
}