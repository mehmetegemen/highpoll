export default {
    'MAIN_MONGO_URL': process.env.MONGO_URL,
    'PORT': process.env.PORT || 3002,
    'USERS_SERVICE_URL': process.env.USERS_SERVICE_URL,
    'SECRET': process.env.SECRET,
    'MAIL_CREDENTIALS': {
        auth: {
            api_key: '41076d24d26c49043919d432c0e87174-b0aac6d0-b93d45d4'
            || process.env.MAIL_API_KEY,
            domain: 'sandboxed97a59eec494f3bb62e1034bc42104f.mailgun.org'
            || process.env.MAIL_DOMAIN
        }
    },
    'HOST_ADDRESS': process.env.HOST_ADDRESS || 'http://localhost:8080/'
}