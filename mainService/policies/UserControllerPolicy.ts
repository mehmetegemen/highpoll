import Joi from 'joi'
import { NextFunction, Response, Request } from 'express';
import providers from '../info/providers'

export default {
    register (req: Request, res: Response, next: NextFunction) {
        const mainSchema = {
            firstName: Joi.string().required(),
            familyName: Joi.string().required(),
            birthDate: Joi.date().required(),
            // RFC 5322 email regex
            email: Joi.string().regex(
                /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
            ).required(),
            // Check that password has
            // ONE upper case letter, ONE lower case letter,
            // ONE digit and ONE special character
            // And at least 8 characters long
            password: Joi.string().regex(
                /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/
            ).required(),
            provider: Joi.string().valid(...providers),
            accessToken: Joi.string()
            .regex(/^[a-zA-Z0-9]+$/)
        }

        const { error } = Joi.validate(req.body, mainSchema);
        if (error) {
            res.send(
                {
                    success: false,
                    message: 'Broken format, check '
                    + error.details[0].context!.key + '.'
                }
            )
        } else {
            next();
        }
    }
}