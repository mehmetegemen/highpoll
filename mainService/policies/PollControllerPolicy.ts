import Joi from 'joi'
import { NextFunction, Response, Request } from 'express';

export default {
    create(req: Request, res: Response, next: NextFunction) {
        const titleSchema = Joi.object(
            {
                label: Joi.string().required(),
                open: Joi.boolean().required()
            }
        )
        
        const detailSchema = Joi.object(
            {
                text: Joi.string(),
                open: Joi.boolean().required()
            }
        )
        
        const votesSchema = Joi.object().keys({
            label: Joi.string().required()
        });

        const mainSchema = Joi.object(
            {
                title: titleSchema.required(),
                detail: detailSchema,
                openToVote: Joi.boolean().required(),
                votes: Joi.array().min(1).max(8).items(votesSchema).required()
            }
        );
        const { error } = Joi.validate(req.body, mainSchema);
        if (error) {
            res.send(
                {
                    success: false,
                    message: 'Broken format, check '
                    + error.details[0].context!.key + '.'
                }
            )
        } else {
            next();
        }
    },
    update (req:Request, res: Response, next: NextFunction) {
        const titleSchema = Joi.object(
            {
                label: Joi.string().required(),
                open: Joi.boolean().required()
            }
        )
        
        const detailSchema = Joi.object(
            {
                text: Joi.string(),
                open: Joi.boolean().required()
            }
        )
        
        const mainSchema = {
            pollId: Joi.string().regex(
                // Accept 10 digit long HEX for pollId
                /^[a-fA-F0-9]{20}$/
            ).required(),
            title: titleSchema,
            detail: detailSchema,
            openToVote: Joi.boolean()
        }
        const { error } = Joi.validate(req.body, mainSchema);
        if (error) {
            res.send(
                {
                    success: false,
                    message: 'Broken format, check '
                    + error.details[0].context!.key + '.'
                }
            )
        } else {
            next();
        } 
    }
}