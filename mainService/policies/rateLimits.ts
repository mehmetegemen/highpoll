import expressRateLimit from 'express-rate-limit'

export const loginLimit = new expressRateLimit({
    // Limit request amount of logins
    windowMs: 1000 * 60, // 1 Minute
    max: 200 // Maximum 200 logins every minute 
});

export const verifyLimit = new expressRateLimit({
    // Limit request amount of logins
    windowMs: 1000 * 60 * 60, // 1 Hour
    max: 50 // Maximum 200 logins every hour 
});

export const registerLimit = new expressRateLimit({
    // Limit request amount of registers
    windowMs: 1000 * 60 * 60 * 2, // 2 hours
    max: 100 // Maximum 100 registers every 2 hour 
});

export const pollCreateLimit = new expressRateLimit({
    // Limit request amount of registers
    windowMs: 1000 * 60 * 60 * 2, // 2 hours
    max: 100 // Maximum 100 poll creation every 2 hour 
});

export const pollUpdateLimit = new expressRateLimit({
    // Limit request amount of registers
    windowMs: 1000 * 60 * 60 * 2, // 2 hours
    max: 300 // Maximum 300 poll updating every 2 hour 
});

export const findByPollIdLimit = new expressRateLimit({
    // Limit request amount of registers
    windowMs: 1000 * 60 * 15, // 15 minutes
    max: 1000 // Maximum 5 finding every 15 minutes 
});

export const destroyLimit = new expressRateLimit({
    // Limit request amount of registers
    windowMs: 1000 * 60 * 60 * 2, // 2 hours
    max: 100 // Maximum 100 poll destruction every 2 hour 
});

export const listByOwnerLimit = new expressRateLimit({
    // Limit request amount of registers
    windowMs: 1000 * 60, // 1 minute
    max: 100 // Maximum 100 poll listing every minute 
});