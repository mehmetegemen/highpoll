import express, { Request, Response, NextFunction } from 'express'
import path from 'path'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import debug from 'debug'
import fs from 'fs'
import cors from 'cors'
import config from './config/config'
import http from 'http'
import routes from './routes'


const app = express();
const server = http.createServer(app);

const log = debug('main-service:app');

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With,' + 
    ' Content-Type, Accept');
    next();
});
app.use(cors());
app.use(express.static(path.join(__dirname, 'public')));

// Handle errors, serve them as JSON
app.use(function(error: any, req: Request, res: Response, next: NextFunction) {
    if (req.app.get('env') === 'development') {
        res.status(500).json(
            {
                success: false,
                message: error.message
            }
        );
    } else {
        res.send('500 - Internal Server Error');
    }
});

require('./passport');
routes(app);

server.listen(config.PORT || 3002);



export default app;