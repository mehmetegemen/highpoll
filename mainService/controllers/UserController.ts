import * as usersRest from '../models/users-rest'
import debug from 'debug'
import config from '../config/config'
import jwt from 'jsonwebtoken'
import { Request, Response } from 'express'
import { IUserModel } from '../schemas/User'

const error = debug('main:error');

function jwtSignVerification (data: any) {
    const ONE_DAY = 60 * 60 * 24;
    return jwt.sign(data, <string>config.SECRET, {
        expiresIn: ONE_DAY
    });
}

export default {
    async register (req: Request, res: Response) {
        try {
            let user: any = await usersRest.find(req.body);
            // Check if user exists
            if (typeof user.success === 'undefined') {
                // User exists
                // Exit with failure
                return res.send(
                    {
                        success: false,
                        message: 'User already exists.'
                    }
                )
            }

            // User does not exist
            // Begin to create
            user = await usersRest.create(req.body);

            // Send verification mail
            await usersRest.sendVerificationMail(
                {
                    to: user.email,
                    token: jwtSignVerification({email: user.email})
                }
            )
            res.send(user);
        } catch (err) {
            error(`Register controller error: ${err.stack}`);
            res.status(500).send(
                {
                    success: false,
                    message: 'Register error.'
                }
            );
        }
    }
}