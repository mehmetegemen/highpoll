import * as usersRest from '../models/users-rest'
import jwt from 'jsonwebtoken'
import config from '../config/config'
import debug from 'debug'
import { Request, Response } from 'express'
import { IUserModel } from '../schemas/User'
import { ExtractJwt } from 'passport-jwt'
const error = debug('main:error');

function jwtSignUser (user: IUserModel) {
    const ONE_YEAR = 60 * 60 * 24 * 365;
    return jwt.sign(user, <string>config.SECRET, {
        expiresIn: ONE_YEAR
    });
}


export default {
    async login (req: Request, res: Response) {
        try {
            if (!req.body.email || !req.body.password) {
                return res.send(
                    {
                        success: false,
                        message: 'Missing credentials.'
                    }
                );
            }
            // Find user in database
            const user: any = await usersRest.find(req.body);
            if (user.success === false) {
                // User is not found
                // Exit with failure
                return res.status(403).send(
                    {
                        success: false,
                        message: 'Login information is incorrect.'
                    }
                );
            }

            // User is found
            // Check if password is valid
            const isPasswordValid = (<any>await usersRest.passwordCheck(req.body)).check;
            if (!isPasswordValid) {
                // Password is invalid
                // Exit with failure
                return res.status(403).send(
                    {
                        success: false,
                        message: 'Login information is incorrect.'
                    }
                )
            }

            // Send User object and Token
            res.send(
                {
                    user,
                    token: jwtSignUser(<IUserModel>user)
                }
            )
        } catch (err) {
            error(`Login controller error: ${err.stack}`)
            res.status(500).send({
                success: false,
                message: 'Login error on server'
            });
        }
    },
    async verify(req: Request, res: Response){
        try {
            const token = ExtractJwt.fromUrlQueryParameter('token')(req);
            if (token===null) {
                return res.send(
                    {
                        success: false,
                        message: 'User could not be verified.'
                    }
                );            
            }
            const decoded: any = await new Promise((resolve, reject) => {
                jwt.verify(token, <string>config.SECRET,
                    (err, decoded) => {
                       if(err) return reject(err);
                       resolve(decoded); 
                    });
            });
    
            let user: any = await usersRest.verify({email: decoded.email});
            if (typeof user === 'string') user = JSON.parse(user);
            // Check if user verified successfuly
            if (typeof user === 'undefined' || !user || user.active===false) {
                // User is not verified
                // Exit with failure
                return res.send(
                    {
                        success: false,
                        message: 'User could not be verified.'
                    }
                );
            }
    
            // User successfuly verified
            // Send user
            res.send(
                {
                    active: user.active
                }
            );            
        } catch (err) {
            res.send(
                {
                    success: false,
                    message: 'Error'
                }
            )
        }
        
    }
}