import * as pollModel from '../models/Poll-mongoose'
import debug from 'debug'
import { Request, Response } from 'express'

const error = debug('main:error');

export default {
    async create (req: Request, res: Response) {
        try {
            // Create poll and assign it
            const poll = await pollModel.createPoll(
                {
                    ...req.body,
                    owner: req.user._id
                }
            );
            res.send(poll);
        } catch (err) {
            error(`createPollController error:${err.stack}`);
            res.status(500).send(
                {
                    success:false,
                    message: 'createPollController error'
                }
            )
        }
    },
    async update (req: Request, res: Response) {
        try {
            let poll = await pollModel.findPollByPollId(
                {pollId: req.body.pollId});
            // Check if poll is not found
            if (poll===null) {
                // Poll is not found
                // Exit with failure
                return res.send(
                    {
                        success: false,
                        message: 'Poll is not found'
                    }
                );
            }
            poll = await pollModel.updatePoll(req.body);
            res.send(poll);
        } catch (err) {
            res.status(500).send(
                {
                    success: false,
                    message: 'updatePollController error'
                }
            );
        }
    },
    async findByPollId (req: Request, res: Response) {
        try {
            // Trim whitespaces and special characters of _id
            req.params.pollId = req.params.pollId.replace(/[^\w]/gi, '');
            if (req.params.pollId.length!==20 
                || !/^[a-fA-F0-9]{20}$/.test(req.params.pollId)){
                return res.send(
                    {
                        success: false,
                        message: 'PollId broken format'
                    }
                )
            }
            const poll = await pollModel.findPollByPollId(
                {pollId: req.params.pollId});
            // Check if poll is not found
            if (poll===null) {
                // Poll is not found
                // Exit with failure
                return res.send(
                    {
                        success: false,
                        message: 'Poll is not found'
                    }
                );
            }
            res.send(poll);
        } catch (err) {
            res.status(500).send(
                {
                    success: false,
                    message: 'findByPollIdPollController error'
                }
            );
        }
    },
    async destroy (req: Request, res: Response) {
        // Trim whitespaces and special characters of _id
        req.params.pollId = req.params.pollId.replace(/[^\w]/gi, '');
        if (req.params.pollId.length!==20 
            || !/^[a-fA-F0-9]{20}$/.test(req.params.pollId)){
            return res.send(
                {
                    success: false,
                    message: 'PollId broken format'
                }
            )
        }
        try {
            const poll = await pollModel.findPollByPollId(
                {pollId: req.params.pollId});
            // Check if poll is not found
            if (poll===null) {
                // Poll is not found
                // Exit with failure
                return res.send(
                    {
                        success: false,
                        message: 'Poll is not found'
                    }
                );
            }
            await pollModel.destroyPoll({pollId: req.params.pollId})
            res.send(
                {
                    message: 'Poll is successfuly deleted'
                }
            );
        } catch (err) {
            res.status(500).send(
                {
                    success: false,
                    message: 'updatePollController error'
                }
            );
        }
    },
    async listByOwner (req: Request, res: Response) {
        // Check if skip and limit are just numbers
        if (!/^[0-9]+$/.test(req.params.skip) 
        || !/^[0-9]+$/.test(req.params.limit)) {
            // Skip or Limit is not just a number
            return res.send(
                {
                    success: false,
                    message: 'Parameters are not numbers'
                }
            );
        }
        const polls = await pollModel.listPollsByOwner(
            {
                owner: req.user._id,
                skip: req.params.skip,
                limit: req.params.limit
            }
        );
        res.send(polls);
    }
}