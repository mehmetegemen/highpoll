export interface IPoll {
    pollId?: string,
    title?: ITitle,
    detail?: IDetail,
    openToVote?: boolean,
    votes?: IVote[],
    viewCount?: number,
    owner?: string,
    [key: string]: any
}

interface IVote {
    label: string,
    count: number
}

interface ITitle {
    label: string,
    open: boolean
}

interface IDetail {
    text: string,
    open: boolean
}