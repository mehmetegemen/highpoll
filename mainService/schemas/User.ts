import { Document, Schema, Model, model } from 'mongoose'
import { IUser } from '../interfaces/User'
const bcrypt = require('bcrypt');

export interface IUserModel extends IUser, Document {};

export let UserSchema: Schema = new Schema({
    birthDate: Date,
    firstName: String,
    familyName: String,
    password: String,
    email: String,
    provider: String,
    accessToken: String
});

export function passwordHash(password: string): Promise<string> {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, 13, function(err: any, hash: string) {
            if (err) return reject(err);
            resolve(hash);
          });
    });
}

export const User: Model<IUserModel> = model<IUserModel>("User", UserSchema);