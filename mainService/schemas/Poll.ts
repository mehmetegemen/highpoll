import { Document, Schema, Model, model } from 'mongoose'
import { IPoll } from '../interfaces/Poll'
import crypto from 'crypto'

export interface IPollModel extends IPoll, Document {}

const voteSchema: Schema = new Schema({
    label: String,
    count: {type: Number, default: 0}
});

export let PollSchema: Schema = new Schema({
    pollId: String,
    title: Object,
    detail: Object,
    openToVote: Boolean,
    votes: [voteSchema],
    owner: String,
    viewCount: {type: Number, default: 0}
},
{
    timestamps: true
}
)

export function generatePollId(): Promise<Buffer> {
    return new Promise<Buffer>((resolve, reject) => {
        crypto.randomBytes(10, function(err, buf) {
            // callback makes crypto async
            if (err) return reject(err);
            resolve(buf);
        })
    })
}

export const Poll: Model<IPollModel> = model<IPollModel>('Poll', PollSchema);