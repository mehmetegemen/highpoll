import passport from 'passport'
import * as usersRest from './models/users-rest'
import { Strategy, ExtractJwt } from 'passport-jwt'
import config from './config/config'

passport.use(
    new Strategy({
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: config.SECRET
    }, async function (jwtPayload, done) {
        try {
            // Find user by email from jwtPayload
            const user = await usersRest.find(
                {
                    email: jwtPayload.email
                }
            );
            if(!user) {
                // There is no user with provided email
                return done(new Error(), false)
            }
            return done(null, user);
        } catch (err) {
            return done(new Error(), err);
        }
    })
);