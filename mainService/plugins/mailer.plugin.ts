import nodemailer from 'nodemailer'
import mailgunTransport from 'nodemailer-mailgun-transport'
import { IMail } from '../interfaces/Mail'
import config from '../config/config'

// Call transport options configuration
const transport = mailgunTransport(config.MAIL_CREDENTIALS);

// Email Service
class EmailService {
    emailClient: any;
    constructor () {
        this.emailClient = nodemailer.createTransport(transport);
    }

    sendHtml (data: IMail) {
        return new Promise((resolve, reject) => {
            this.emailClient.sendMail(
                {
                    from: data.from,
                    to: data.to,
                    subject: data.subject,
                    html: data.html
                }, (err: any, info: any) => {
                    if (err) reject (err);
                    else resolve (info);
                }
            )
        })
    }
}

export default new EmailService();